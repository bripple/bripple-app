import React from 'react';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import DeedScreen from '../screens/DeedScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
}) 

const DeedStack = createStackNavigator({
  Deed: DeedScreen,
})

DeedStack.navigationOptions = {
  tabBarLabel: 'Create Good Deed',
  tabBarIcon: ({ focused }) => {
    <tabBarIcon
      focused={focused}
      name={
        `iso-link${focused ? '' : '-outline'}`
      }
    />
  },
};

export default createBottomTabNavigator({
  //HomeStack,
  DeedStack,
});