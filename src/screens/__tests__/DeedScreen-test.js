import "react-native";
import React from "react";
import { shallow } from 'enzyme';
import { DeedScreen } from "../DeedScreen";

// FIXME: console.log bug in jest: https://github.com/facebook/jest/issues/3853

function workaroundLog() {
  console.log('');
  console.log('');
  console.log('');
}

test('Dummy test', () => {
  expect(1+1).toBe(2);
})

test('Deed screen creation', () => {
  const deedScreen = shallow(<DeedScreen />);
  console.log(deedScreen);
  expect(deedScreen.find('ScrollView').exists()).toBe(true);
  workaroundLog();
})

test('Trigger createDeed action',  () => {
  const deedScreen = shallow(<DeedScreen />);
  //console.log(deedScreen.childAt(3).childAt(1).props.createDeem({}));
  
  workaroundLog();
})

/*
it("renders the deed screen", async () => {
  const tree = renderer.create(<DeedScreen />).toJSON();
  expect(tree).toMatchInlineSnapshot(`
<RCTScrollView
  style={
    Object {
      "backgroundColor": "#fff",
      "flex": 1,
    }
  }
>
  <View>
    <View
      style={
        Object {
          "alignItems": "center",
        }
      }
    >
      <Text
        accessible={true}
        allowFontScaling={true}
        ellipsizeMode="tail"
        style={
          Object {
            "fontSize": 22,
            "paddingTop": 20,
          }
        }
      >
        Create My Good Deed to Forward
      </Text>
    </View>
    <View>
      <RCTPicker
        items={
          Array [
            Object {
              "label": "Love Message",
              "textColor": undefined,
              "value": "loveMessage",
            },
            Object {
              "label": "Service",
              "textColor": undefined,
              "value": "Service",
            },
          ]
        }
        onChange={[Function]}
        onResponderTerminationRequest={[Function]}
        onStartShouldSetResponder={[Function]}
        selectedIndex={0}
        style={
          Array [
            Object {
              "height": 216,
            },
            Object {
              "backgroundColor": "white",
              "height": 150,
            },
          ]
        }
      />
    </View>
    <View
      style={
        Object {
          "alignItems": "center",
        }
      }
    >
      <TextInput
        allowFontScaling={true}
        multiline={true}
        onChangeText={[Function]}
        style={
          Object {
            "alignItems": "center",
            "borderColor": "gray",
            "borderWidth": 1,
            "fontSize": 20,
            "height": 200,
            "width": 300,
          }
        }
        value=""
      />
    </View>
    <View
      style={
        Object {
          "alignItems": "center",
          "flex": 1,
          "flexDirection": "row",
          "justifyContent": "center",
          "paddingTop": 20,
        }
      }
    >
      <View
        style={
          Object {
            "flex": 1,
            "marginRight": 10,
          }
        }
      >
        <View
          style={
            Array [
              Object {
                "marginLeft": 15,
                "marginRight": 15,
              },
              undefined,
              undefined,
              undefined,
            ]
          }
        >
          <View
            accessible={true}
            isTVSelectable={true}
            onResponderGrant={[Function]}
            onResponderMove={[Function]}
            onResponderRelease={[Function]}
            onResponderTerminate={[Function]}
            onResponderTerminationRequest={[Function]}
            onStartShouldSetResponder={[Function]}
            style={null}
          >
            <View
              pointerEvents="box-only"
              style={
                Array [
                  Object {
                    "alignItems": "center",
                    "backgroundColor": "#9E9E9E",
                    "flexDirection": "row",
                    "justifyContent": "center",
                    "padding": 19,
                  },
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  Object {
                    "padding": 12,
                  },
                  undefined,
                  undefined,
                  undefined,
                  Object {
                    "backgroundColor": "grey",
                    "borderRadius": 5,
                  },
                  undefined,
                  undefined,
                ]
              }
            >
              <Text
                accessible={true}
                allowFontScaling={false}
                ellipsizeMode="tail"
                style={
                  Array [
                    Object {
                      "color": "white",
                      "fontSize": 18,
                    },
                    Array [
                      Object {
                        "marginRight": 10,
                      },
                      undefined,
                    ],
                    Object {
                      "fontFamily": "Material Icons",
                      "fontStyle": "normal",
                      "fontWeight": "normal",
                    },
                  ]
                }
              >
                
              </Text>
              <Text
                accessible={true}
                allowFontScaling={true}
                ellipsizeMode="tail"
                style={
                  Array [
                    Object {},
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    Array [
                      Object {
                        "color": "white",
                        "fontSize": 20,
                      },
                      undefined,
                      Object {
                        "fontSize": 17.5,
                      },
                      undefined,
                      undefined,
                      undefined,
                      undefined,
                      undefined,
                    ],
                  ]
                }
              >
                Clear Message
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={
          Object {
            "flex": 1,
          }
        }
      >
        <View
          style={
            Array [
              Object {
                "marginLeft": 15,
                "marginRight": 15,
              },
              undefined,
              undefined,
              undefined,
            ]
          }
        >
          <View
            accessible={true}
            isTVSelectable={true}
            onResponderGrant={[Function]}
            onResponderMove={[Function]}
            onResponderRelease={[Function]}
            onResponderTerminate={[Function]}
            onResponderTerminationRequest={[Function]}
            onStartShouldSetResponder={[Function]}
            style={null}
          >
            <View
              pointerEvents="box-only"
              style={
                Array [
                  Object {
                    "alignItems": "center",
                    "backgroundColor": "#9E9E9E",
                    "flexDirection": "row",
                    "justifyContent": "center",
                    "padding": 19,
                  },
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  undefined,
                  Object {
                    "padding": 12,
                  },
                  undefined,
                  undefined,
                  undefined,
                  Object {
                    "backgroundColor": "#00aeef",
                    "borderRadius": 5,
                  },
                  undefined,
                  undefined,
                ]
              }
            >
              <Text
                accessible={true}
                allowFontScaling={false}
                ellipsizeMode="tail"
                style={
                  Array [
                    Object {
                      "color": "white",
                      "fontSize": 18,
                    },
                    Array [
                      Object {
                        "marginRight": 10,
                      },
                      undefined,
                    ],
                    Object {
                      "fontFamily": "Material Icons",
                      "fontStyle": "normal",
                      "fontWeight": "normal",
                    },
                  ]
                }
              >
                
              </Text>
              <Text
                accessible={true}
                allowFontScaling={true}
                ellipsizeMode="tail"
                style={
                  Array [
                    Object {},
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    Array [
                      Object {
                        "color": "white",
                        "fontSize": 20,
                      },
                      undefined,
                      Object {
                        "fontSize": 17.5,
                      },
                      undefined,
                      undefined,
                      undefined,
                      undefined,
                      undefined,
                    ],
                  ]
                }
              >
                Create Deed
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  </View>
</RCTScrollView>
`);
});
*/