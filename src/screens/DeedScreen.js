import React from 'react';
import {
  ScrollView,
  Picker,
  StyleSheet,
  Text,
  View,
  TextInput,
} from 'react-native';
import { Button } from 'react-native-elements';
import { createDeed } from '../redux/actions/deemActions';
import { connect } from 'react-redux';

export class DeedScreen extends React.Component {
  static navigationOptions = {
    title: 'My Good Deed',
  };

  constructor(props) {
    super(props);
    this.state = {
      type: '',
      text: ''
    };
  }

  _create() {
    console.log('test');
  }

  _cancel() {
    //this._textInput.setNativeProps({value: ''});
    //this._textInput.clear();
  }

  render() {
    const props = this.props;

    return (
      <ScrollView style={styles.container}>
        <View style={styles.deedContainer}>
          <Text style={styles.title}>Create Good Deed to Forward</Text>
        </View>
        <Picker
          itemStyle={{ backgroundColor: 'white', height:150 }}
          itemTextStyle={{ fontSize: 15, color: 'white' }}
          selectedValue={this.state.type}
          onValueChange={(itemValue) => this.setState({type: itemValue})}
          >
          <Picker.Item label="Love Message" value="message" />
          <Picker.Item label="Service" value="service" />
        </Picker>
        <View style={styles.deedContainer}>
          <TextInput ref={component => this._textInput = component}
            style={styles.deedTextInput}
            multiline={true}
            onChangeText={(text) => this.setState({text: text})}
            value=""
          />
        </View>
        <View style={{paddingTop:20, flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <View style={{flex:1 , marginRight:10}} >
            <Button
              icon={{name: 'clear'}}
              buttonStyle={styles.cancelButton}
              title="Clear Message"
              onPress={() => this._cancel()}
            />
          </View>
          <View style={{flex:1}} >
            <Button
              icon={{name: 'send'}}
              buttonStyle={styles.createButton}
              title="Create Deed"
              onPress={() => props.createDeed({...this.state})}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    paddingTop:20,
    fontSize: 22,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  deedContainer: {
    alignItems: 'center',
  },
  deedTextInput: {
    fontSize: 20,
    alignItems: 'center',
    width: 300,
    height: 200,
    borderColor: 'gray',
    borderWidth: 1
  },
  cancelButton: {
    backgroundColor: 'grey',
    borderRadius: 5
  },
  createButton: {
    backgroundColor: '#00aeef',
    borderRadius: 5
  }
});

function mapStateToProps(state) {
  return {
    type: state.type,
    text: state.text
  }
}

function mapDispatchToProps(dispatch) {
  return {
    createDeed: (data) => dispatch(createDeed(data))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeedScreen)