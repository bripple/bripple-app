import * as actionType from './types';

export function createDeed(data) {
  return {
    type: actionType.CREATE_DEED,
    data: data
  }
}