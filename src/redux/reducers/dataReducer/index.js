import {
  FETCHING_DATA,
  FETCHING_DATA_SUCCESS,
  FETCHING_DATA_FAILURE
} from '../../actions/types';

const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false
}

export default function dataReducer(state = initialState, action) {
  switch(action.type) {
    case FETCHING_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
        dataFetched: false
      }
    case FETCHING_DATA_SUCCESS:
      return {
        ...state,
        data: action.data,
        isFetching: false,
        dataFetched: true
      }
    case FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        dataFetched: false,
        error: true
      }
    default:
      return state;
  }
}