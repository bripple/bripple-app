import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';

import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';

const sagaMiddleware = createSagaMiddleware();
const logger = createLogger();
const middlewares = [sagaMiddleware, logger];

const store = createStore(rootReducer, applyMiddleware(...middlewares));
//sagaMiddleware.run();

export default store;