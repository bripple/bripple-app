const path = require('path')
const protobuf = require("protobufjs");

function rpcImpl(method, requestData, callback) {
  // perform the request using an HTTP request or a WebSocket for example
  var responseData = 'test';
  console.log(method);
  console.log(requestData);
  // and call the callback with the binary response afterwards:
  callback(null, responseData);
}

console.log(path.resolve('./deed_service.proto'));

export default function loadProtobuf(done) {
  protobuf.load(path.resolve('src/proto/deed_service.proto'))
    .then((root) => {
      console.log(root);
      let deedService = root.lookup('DeedService').create(rpcImpl, false, false);
      console.log(deedService);
      let deed = deedService.createDeed({name: 'testName', DeedType: '1'});
      console.log(deed);
      done();
    })
    .catch((err) => {
      console.log(err);
    })
}
